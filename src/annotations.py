"""
Date: 13 Nov. 2019
Author: Xiao Yang <yangx@ebi.ac.uk>
"""

import requests
import lxml.etree as etree
import re
from typing import List, Tuple, Dict, Set, Iterator, Any, Union
from collections import defaultdict
import csv
import logging
from copy import deepcopy
import os
import json

logging.basicConfig(level=logging.INFO, filename='annotations.log')
logger = logging.getLogger(__name__)


def retrieve_fulltext(pmcid: str) -> str:
    """
    Use EuropePMC webservice to get fulltext xml given pmcid
    :param pmcid: article pmcid
    :type pmcid: str
    :return: fulltext xml
    :rtype: str
    """
    fulltext_url = f'https://www.ebi.ac.uk/europepmc/webservices/rest/{pmcid}/fullTextXML'
    response = requests.get(fulltext_url)
    response.raise_for_status()
    fulltext_xml = response.text
    return fulltext_xml


def extract_text_sents(sents_xml: str, blacklist: Union[Tuple[str], List[str], Set[str]] = ()) \
        -> List[Tuple[int, str, str]]:
    """
    if sentence in certain sections, skip the sentence.
    the removed sentences can affect the regex match
    because it might be in the search context
    :param sents_xml: sentencised XML files
    :type sents_xml: str
    :param blacklist: a list of section names (SecTag)
    :type blacklist: Union[Tuple[str], List[str], Set[str]]
    :return: a list of the selected sentences [sid, sentence, section name]
    :rtype: List[Tuple[int, str, str]]
    """
    root = etree.fromstring(sents_xml)
    sents = root.xpath("//plain")
    fulltext = []
    for sid, sent in enumerate(sents):
        parents = list(sent.iterancestors(tag='SecTag'))

        sec = parents[0].get('type') if parents else 'UNK'

        # check if we need to skip the sentence
        if parents:
            skip = False
            while parents and not skip:
                if parents.pop().get('type') in blacklist:
                    skip = True
            if skip:
                continue

        texts = sent.itertext()
        fulltext.append((int(sid), ''.join(texts), sec))
    return fulltext


def find_correct_span(comment: str, exact: str, prefix: str, suffix: str,
                      corrections: Dict[str, str]) -> Union[Tuple[str, str, str], None]:
    """
    if the correct span is noted in the comment box, find the correct span using available annotation information
    This is used to tackle the inconsistency of annotations made by our annotators,
    however this may not be able to cover all cases.

    :param comment: comment made by some annotators
    :type comment: str
    :param exact: the annotation
    :type exact: str
    :param prefix: the prefix of annotation
    :type prefix: str
    :param suffix: the suffix of annotation
    :type suffix: str
    :param corrections: key values pairs for annotations that needs to be corrected/modified
    :type corrections: Dict[str, str]
    :return: modified annotation, prefix and suffix, if cannot find return None
    :rtype: Union[Tuple[str, str, str], None]
    """
    # some wrong span tags are annotated in the comment box with format: "Correct span: XXX XXX"
    # adjust the prefix and suffix for such cases
    correct_match = re.search(pattern=r'[Cc]orrect span: "(.+)"', string=comment)
    if correct_match:
        correct_exact = correct_match.group(1)
    else:
        return None

    if correct_exact in corrections:
        correct_exact = corrections[correct_exact]
    match = re.search(pattern=r'(.*){}(.*)'.format(re.escape(correct_exact)), string=prefix+exact+suffix)

    if match:
        correct_prefix = match.group(1)
        correct_suffix = match.group(2)
    else:
        logger.debug('cannot find correct span...')
        # logger.debug(str(err))
        logger.debug(f'the correct span is: {correct_exact}\ncontext: {prefix+exact+suffix}')
        # Todo: correct span is none due to input.
        return None
    return correct_exact, correct_prefix, correct_suffix


def generate_tag_all_patterns(exact: str):
    """
    generate pattern for annotations with 'ALL' tag
    :param exact: the annotation
    :type exact: str
    :return: escaped regex pattern
    :rtype: str
    """
    # if tag all occurrences, we dont know the prefix or suffix
    # hence let them be word boundaries
    # lowercase the pattern when tag_all=Teue so we can match all forms of the pattern e.g. Hepotic vs hepotic
    # TODO: will this introduce other problems?
    patterns = []
    # patterns.append(r'[^a-zA-Z0-9]({0})[^a-zA-Z0-9]'.format(re.escape(exact.strip())))
    # patterns.append(r'^({0})[^a-zA-Z0-9]'.format(re.escape(exact.strip())))
    # patterns.append(r'[^a-zA-Z0-9]({0})$'.format(re.escape(exact.strip())))
    # patterns.append(r'^({0})$'.format(re.escape(exact.strip())))
    patterns.append(r'\b({0})\b'.format(re.escape(exact.strip())))
    return patterns


def generate_patterns(prefix: str, exact: str, suffix: str) -> Tuple[str, str, str]:
    """
    generate search patterns using prefix, exact and suffix
    return a pattern with prefix, exact and suffix, a pattern with only prefix and exact,
    a pattern with open exact and suffix

    The extracted sentences from core pipeline sometimes are different with Hypothes.is. Thus, using exact, prefix and
    suffix from Hypothes.is may miss some annotations on XML sentences from core piepline.
    The additional patterns are supposed to extract more annotations even if there is inconsistency between
    Core pipeline and Hypothes.is
    """
    # exact may have space included we need to remove the space e.g. " bear "
    # then adjust the prefix and suffix to include the corresponding spaces if necessary
    check_pattern = prefix + exact + suffix
    if exact.strip() != exact:
        match = re.search(pattern=r'({})'.format(re.escape(exact.strip())), string=exact)
        if not match:
            raise Exception('No matching found...')
        match_text = match.group(1)
        match_span = match.span(1)
        pre_match = exact[0:match_span[0]]
        post_match = exact[match_span[1]:]
        prefix = prefix + pre_match
        suffix = post_match + suffix
        exact = match_text

    if prefix+exact+suffix != check_pattern:
        # as we didn't remove any information for now,
        # the original pattern and the adjusted pattern should be the same
        raise
    # however, the sentrenciser did separate exact and suffix in some cases
    # this is to remove return within a sentence.
    # prefix = prefix.replace('\r', '').replace('\n', '')
    # exact = exact.strip('\r\n')
    # suffix = suffix.strip('\r\n')

    # hypothesis doesn't have space for things like "24 mm" or "citation [23]"
    # by tweaking prefix, assume pattern_prefix_only will pick up such case
    # add space before citation
    prefix = re.sub(pattern=r'(\w)(\[\d{1,3}\])', repl=r'\1 \2', string=prefix)
    # add space between number and metric mm
    # TODO: we may need to add more metric units for better coverage
    prefix = re.sub(pattern=r'(\d+)(mm)', repl=r'\1 \2', string=prefix)
    # remove return carriages and newline
    prefix = re.sub(pattern=r'[\r\n]', repl='', string=prefix)

    # replace unicode special white space to standard white space
    # match with search contexts
    exact = exact.replace('\u2009', ' ')
    prefix = prefix.replace('\u2009', ' ')
    suffix = suffix.replace('\u2009', ' ')

    pattern = re.escape(prefix[-20:]) + '(' + re.escape(exact) + ')' + re.escape(suffix[:5])
    pattern_prefix_only = re.escape(prefix[-20:]) + '(' + re.escape(exact) + ')'
    pattern_suffix_only = '(' + re.escape(exact) + ')' + re.escape(suffix[:20])

    return pattern, pattern_prefix_only, pattern_suffix_only


def generate_context(sent_idx: int, sent: str, fulltext: List[str], tag_all: bool) -> Tuple[List, str]:
    """
    generate contexts for regex to search from,
    current 4 contexts cover 4 cases
    (1) just concatenate pre_sent, sent and post_sent
    (2) remove trailing chars of the pre_sent
    (3) remove trailing chars of the sent
    (4) remove trailing chars of both pre_sent and sent
    trailing chars such as return carriages will be removed
    :param sent_idx: current sent index
    :type sent_idx: int
    :param sent: current sent
    :type sent: str
    :param fulltext: list of all the sentences available
    :type fulltext: list
    :param tag_all: is the pattern is tag_all case
    :type tag_all: bool
    :return: list of available contexts
    :rtype: list
    """
    try:
        pre_sent = fulltext[sent_idx - 1]
    except IndexError as err:
        logger.debug(str(err))
        pre_sent = ''
    try:
        post_sent = fulltext[sent_idx + 1]
    except IndexError as err:
        logger.debug(str(err))
        post_sent = ''

    # avoid matching words in pre_sent and post_sent
    if tag_all:
        pre_sent = ''
        post_sent = ''

    search_context1 = pre_sent[-30:] + sent + post_sent[:30]
    search_context2 = pre_sent[-30:].rstrip() + sent + post_sent[:30]
    search_context3 = pre_sent[-30:] + sent.rstrip() + post_sent[:30]
    search_context4 = pre_sent[-30:].rstrip() + sent.rstrip() + post_sent[:30]

    search_contexts = [search_context1, search_context2, search_context3, search_context4]

    # replace unicode special white space to standard white space
    for i, context in enumerate(search_contexts):
        search_contexts[i] = context.replace('\u2009', ' ')

    return search_contexts, pre_sent


def load_dict_csv(csv_path: str) -> csv.DictReader:
    """
    read csv file that has header
    :param csv_path:
    :type csv_path:
    :return:
    :rtype:
    """
    with open(csv_path, 'r') as f:
        annotation_data = f.readlines()
    return csv.DictReader(annotation_data)


def ignore_cases_tag_all(exact: str, ignore_list: List = None):
    """
    if exact term is title, ignore the cases when doing RE match
    # e.g. Rats, Human
    # but dont ignore cases such as HPV, HIV, CaN

    if exact is in an ignore list, return True to ignore the cases
    else let istitle() to check if ignore cases
    """
    exact = exact.strip()
    # check ignore list first, if not in ignore list, continue to other checks
    if ignore_list:
        ignore_set = set(ignore_list)
        if exact in ignore_set:
            return True

    # if 'exact' is a phrase, we assume we can safely ignore cases
    # if 'exact' is a single word, check if .istitle()
    if len(exact.split()) > 1:
        return True
    elif '-' in exact or '_' in exact or '/' in exact or '(' in exact:
        return True
    elif exact.isalnum() and not exact.isalpha():
        return True
    else:
        return exact.istitle() or exact.islower()
    # return exact.strip().lower() != 'can'


def annotation_matching(pre_sent: str, sent: str, search_contexts: List[str], tag_all: bool,
                        exact: str, ignore_list: List = None, *kwargs) -> Tuple[List, int, int]:
    """
    match patterns from the given sentence
    using available search contexts and patterns

    :param pre_sent:
    :type pre_sent:
    :param sent:
    :type sent:
    :param search_contexts:
    :type search_contexts:
    :param tag_all:
    :type tag_all:
    :param exact:
    :type exact:
    :param ignore_list:
    :type ignore_list:
    :param kwargs:
    :type kwargs:
    :return:
    :rtype:
    """
    patterns = kwargs

    context_idx = 0
    matches = []

    offset = None
    sent_length = None

    while (not matches) and (context_idx < len(search_contexts)):
        search_context = search_contexts[context_idx]

        # add ignore_cases_tag_all to ignore cases in certain cases when tag_all=True
        # so we can match all forms of the pattern e.g. Hepotic vs hepotic
        for p in patterns:
            if tag_all:
                if ignore_cases_tag_all(exact, ignore_list):
                    matches = list(re.finditer(pattern=p, string=search_context, flags=re.IGNORECASE))
                else:
                    matches = list(re.finditer(pattern=p, string=search_context))
            else:
                matches = list(re.finditer(pattern=p, string=search_context))
            if matches:
                break

        # since we added pre sentence to the context, we need to know the length of pre sentence
        # to it them later e.g. offset
        if context_idx == 1 or context_idx == 3:
            offset = len(pre_sent[-30:].rstrip())
        else:
            offset = len(pre_sent[-30:])

        # since in some contexts, the trailing chars are removed from current sentence,
        # we need to keep track the length of sentence
        if context_idx == 2 or context_idx == 3:
            sent_length = len(sent.rstrip())
        else:
            sent_length = len(sent)

        context_idx = context_idx + 1
    return matches, offset, sent_length


def match_annotation_sents(annotation_path: str, sentencised_path: str,
                           sec_blacklist: Tuple[str, ...] = (), corrections: Dict[str,str] = (),
                           reannotate: bool = False, ignore_list: List = None, annot_blacklist: Tuple[str, ...] = ())\
        -> Tuple[Dict[int, List[Dict]], int, List[Tuple]]:
    """
    match annotations from all the fulltext sentences in an article

    :param annotation_path: annotations in csv format
    :type annotation_path: str
    :param sentencised_path: path to the sentencised xml
    :type sentencised_path: str
    :param sec_blacklist: blacklist for sections
    :type sec_blacklist:
    :param corrections:
    :type corrections:
    :param reannotate:
    :type reannotate:
    :param ignore_list:
    :type ignore_list:
    :param annot_blacklist: blacklist for annotations
    :type annot_blacklist:
    :return: dict contains sentence idx as key and a list of the matches as value
    :rtype: dict
    """

    missed_patterns = []
    logger.info('=========='*5 + '\n')
    logger.info(f'loading annotation from: {annotation_path}')
    logger.info(f'loading sentencised xml from: {sentencised_path}')

    pmcid = os.path.basename(annotation_path).split('-')[0]

    # load csv annotation
    anno_df = load_dict_csv(annotation_path)
    # match annotations by sents
    with open(sentencised_path, 'r') as f:
        sent_xml = f.read()
    fulltext_sents = extract_text_sents(sent_xml, blacklist=sec_blacklist)
    logger.info(f'{len(fulltext_sents)} sentences extracted from {pmcid}')
    doc_length = len(fulltext_sents)

    sent_to_match = defaultdict(list)
    for row in anno_df:
        pattern_found = False
        exact = row['exact']
        prefix = row['prefix']
        suffix = row['suffix']
        tag = row['tags']
        comment = row['comment']

        prefix = prefix.rstrip('\r\n')

        # occasionally, an exact mach could be an empty string
        # it has to be skipped
        if not exact.strip():
            continue

        # TODO: allow correcting the typos with a list
        # correct typos
        # repalce '-' with '_'
        # sometimes annotators make typos such as MIS-OG which should be WT_OG
        tag = tag.replace('-', '_')
        tag = tag.replace('ABG', 'AMB')
        tag = tag.replace('DIS', 'DS')
        tag = tag.replace('{', '')
        tag = tag.replace('}', '')

        # reanotate types such as MIS_GP as GP
        if reannotate:
            tag = reannotate_type(tag)
            # after reannotate if the tag is removed (i.e. false positive), skip this annotations
            if not tag:
                continue

        tag_all = (row['all'] == 'yes')
        pattern_count = 0

        if comment.strip():
            correct_span = find_correct_span(comment, exact, prefix, suffix,
                                             corrections=corrections)
            if correct_span:
                exact, prefix, suffix = correct_span
        if tag_all:
            patterns = generate_tag_all_patterns(exact)
        else:
            patterns = generate_patterns(prefix, exact, suffix)

        fulltext = list(list(zip(*fulltext_sents))[1])

        for idx, sent, sec in fulltext_sents:
            if exact.strip().lower() in sent.lower():

                search_contexts, pre_sent = generate_context(idx, sent, fulltext, tag_all=tag_all)
                matches, offset, sent_length = annotation_matching(pre_sent, sent, search_contexts, tag_all,
                                                                   exact, ignore_list, *patterns)

                if matches:
                    for match in matches:
                        # search context is offseted because of pre_sents
                        if tag_all:
                            # remove this to recover all from original sent and indices
                            match_span = [index - offset for index in match.span(1)]
                            match_text = sent[match_span[0]:match_span[1]]
                        else:
                            match_text = match.group(1)
                            match_span = [index - offset for index in match.span(1)]

                        # skip the annotation if it is in the annotation blacklist
                        if match_text in annot_blacklist:
                            continue

                        # skip the match in the pre_sent or post sent
                        if match_span[0] < 0 or match_span[1] > sent_length:
                            # print("match in the pre/post context")
                            continue

                        sent_to_match[idx].append(
                            {'sent': sent, 'text': match_text, 'span': match_span, 'tag': tag, 'all': tag_all,
                             'pattern': match.re.pattern, 'section': sec})

                        assert match_text == sent[match_span[0]:match_span[1]], "match span doesn't yield the match text"

                        pattern_count += 1
                        pattern_found = True

        if pattern_found:
            missed_patterns.append((prefix, exact, suffix, tag, tag_all, 'yes'))
            if pattern_count > 1:
                logger.debug(f'same pattern found in {pattern_count} different sentences')
                logger.debug(f'exact: {exact}, prefix: {prefix}, suffix: {suffix}')
        else:
            missed_patterns.append((prefix, exact, suffix, tag, tag_all, 'no'))
            logger.debug('no matches')
            logger.info('----------'*5 + '\n')
            logger.info(f'no matches, pmcid: {pmcid}')
            logger.info(f'exact: {exact}, prefix: {prefix}, suffix: {suffix}, tag all: {tag_all}, tag: {tag}')
            logger.info(f'string: {prefix+exact+suffix}')
            if not tag_all:
                logger.info('all the patterns failed to match: '
                            f'\npattern: {patterns[0]}'
                            f'\npattern_prefix_only: {patterns[1]}'
                            f'\npattern_suffix_only: {patterns[2]}'
                            )
    return sent_to_match, doc_length, missed_patterns


def remove_sub_string_match(sent_to_match: Dict[int, List[Dict]]) -> Dict[int, List]:
    """
    resolve annotations that are overlaped

    :param sent_to_match:
    :type sent_to_match:
    :return:
    :rtype:
    """
    sent_to_match_copy = deepcopy(sent_to_match)
    for key in sent_to_match:
        annotations = sent_to_match_copy[key]
        # a set to remove possible duplicate index
        # e.g. annotation contain [recombinase[all], recombinase[pattern], Cre recombinase]
        removal_index = set()
        for i, anno1 in enumerate(annotations):
            if i+1 < len(annotations):
                # skip relation annotations because entity may overlap with relation annotation,
                # which lead entity being removed
                if anno1['tag'] in ('NGD', 'YGD', 'AMB'):
                    continue
                for j,anno2 in enumerate(annotations[i+1:]):
                    # skip relation annotations
                    if anno2['tag'] in ('NGD', 'YGD', 'AMB'):
                        continue
                    if list(range(max(anno1['span'][0], anno2['span'][0]), min(anno1['span'][1], anno2['span'][1]))):
                        logger.debug(f'sent idx: {key}')
                        logger.debug(f"overlap between {anno1['text']} and {anno2['text']}")
                        logger.debug(annotations)
                        if anno1['text'] in anno2['text']:
                            if anno1['text'] == anno2['text']:
                                # if anno1 is not tagged with "ALL"
                                if not anno1['all'] and anno2['all']:
                                    removal_index.add(i+1+j)
                                # if anno1 is tagged with "ALL"
                                elif anno1['all'] and not anno2['all']:
                                    removal_index.add(i)
                                # resolve the case both are tagged as with [ALL] e.g. [CRT_OG,ALL] and [MIS_OG][ALL]
                                elif anno1['all'] and anno2['all']:
                                    if 'MIS' in anno1['tag']:
                                        removal_index.add(i)
                                    else:
                                        removal_index.add(i+1+j)
                                else:
                                    logger.info(f"different tags {anno1['tag']} vs {anno2['tag']} for the same entity {anno1['text']}")
                            else:
                                removal_index.add(i)
                        elif anno2['text'] in anno1['text']:
                            removal_index.add(i+1+j)
                        else:
                            logger.info(f"only partially overlap: {anno1['text']} vs. {anno2['text']}")
        for rm_idx in sorted(removal_index, reverse=True):
            del annotations[rm_idx]
    return sent_to_match_copy


def reannotate_type(tag):
    """Convert WT to the correspoding tag
    if it is false positive, tag it as FP
    """
    tag = tag.upper()
    tags = tag.split(',')
    if 'ALL' in tags:
        tags.remove('ALL')

    if 'MIS_' in tag or 'CRT_' in tag or 'WS_' in tag:
        # tags = tag.split(',')
        if len(tags)==1:
            reannotated_tag = tags[0].split('_')[1]
        else:
            if 'MIS_' in tags[0] or 'CRT_' in tags[0] or 'WS_' in tags[0]:
                reannotated_tag = tags[0].split('_')[1]
            else:
                reannotated_tag = tags[1].split('_')[1]
    elif 'WT_' in tag:
        # tags = tag.split(',')
        if len(tags) > 1:
            if 'WT_' in tags[0]:
                reannotated_tag = tags[1]
            else:
                reannotated_tag = tags[0]
        else:
            reannotated_tag = ''
    elif 'AMB' in tag or 'NGD' in tag or 'YGD' in tag:
        reannotated_tag = tag
    else:
        raise ValueError(f'Unexpected tag {tag} received!')
    if reannotated_tag == 'ALL':
        print(tag)
        raise
    return reannotated_tag


def preprocess_csv_annotations(annotation_dir: str, sentencised_xml_dir: str,
                               sec_blacklist: Tuple[str, ...]=(), corrections: Dict[str,str]=(),
                               reannotate: bool = False, ignore_list: List = None,
                               annot_blacklist:Tuple[str, ...]=()) -> Dict[str, List[Dict]]:
    """

    :param annotation_dir:
    :type annotation_dir:
    :param sentencised_xml_dir:
    :type sentencised_xml_dir:
    :param sec_blacklist: section black list
    :type sec_blacklist:
    :param corrections:
    :type corrections:
    :param reannotate:
    :type reannotate:
    :param ignore_list:
    :type ignore_list:
    :param annot_blacklist: blacklist for annotations
    :type annot_blacklist:
    :return:
    :rtype:
    """
    try:
        sentencised_files = os.listdir(sentencised_xml_dir)
        annotation_fpaths = []
        for root, dirs, files in os.walk(annotation_dir):
            for name in files:
                if '.csv' in name:
                    annotation_fpaths.append(os.path.join(root,name))
    except NotADirectoryError as err:
        logger.exception(str(err))
        raise
    except FileNotFoundError as err:
        logger.exception(str(err))
        raise
    pmcid_sent_fpath = {fname.split('.')[0]:os.path.join(sentencised_xml_dir, fname)  for fname in sentencised_files}

    aligned_doc = defaultdict(list)

    missed_patterns_all = defaultdict(list)

    for anno_fpath in annotation_fpaths:
        pmcid = os.path.basename(anno_fpath).split('-')[0]
        annotator = os.path.basename(anno_fpath).split('-')[1]
        sent_fpath = pmcid_sent_fpath[pmcid]
        sent_match, doc_length, missed_patterns = match_annotation_sents(anno_fpath, sent_fpath, sec_blacklist,
                                                                         corrections, reannotate, ignore_list,
                                                                         annot_blacklist)

        sent_match = remove_sub_string_match(sent_match)
        missed_patterns_all[pmcid].append((annotator, missed_patterns))
        aligned_doc[pmcid].append({'annotator': annotator, 'annotation': sent_match, 'doc_length': doc_length})

    # # Save the missed patterns for further anlysis
    # with open('missed_patterns.csv', 'w') as f:
    #     dictwriter = csv.DictWriter(f, fieldnames=['pmcid', 'annotator', 'prefix', 'exact', 'suffix', 'tag', 'all', 'matched'])
    #     dictwriter.writeheader()
    #     for pmcid in missed_patterns_all:
    #         for annotator, missed in missed_patterns_all[pmcid]:
    #             for prefix, exact, suffix, tag, tag_all, matched in missed:
    #                 dictwriter.writerow({'pmcid': pmcid, 'annotator': annotator, 'prefix': prefix, 'exact': exact, 'suffix': suffix, 'tag': tag, 'all': tag_all, 'matched': matched})

    return aligned_doc


if __name__ == '__main__':
    annotation_dir = 'annotations/hypothesis/csv'
    sentencised_dir = 'articles/Sentencised/full_sent'

    section_blacklist = ('REF', 'ACK_FUND')
    corrections = {"Marek's disease tumor":"Marek’s disease tumor"}

    case_ignore_list = ['EBV', 'BAX', 'MomL', 'ERK', 'MEK', 'SRC', 'PTEN', 'SMAD', 'ptxS', 'rpoEs', 'groES', 'groEL',
                        'nAChR', 'MYC', 'RXRα', 'MAPK', 'csrD', 'ARU', 'TRIS', 'proBDNF', 'ciaB', 'TS', 'HSPs', 'GP',
                        'BDNF', 'HTT', 'MIR', 'FES', 'TaTypA', 'VECad', 'CLIC', 'CHS', 'CAB', 'BRCA', 'NTA', 'LRE',
                        'CLOCK', 'WAVE']
    # provides another option to remove particular annotations. useful when ignore cases with regex or case ignore list
    # is too long/difficult to generate
    annotation_blacklist = ()
    aligned_docs = preprocess_csv_annotations(annotation_dir, sentencised_dir, sec_blacklist=section_blacklist,
                                              corrections=corrections, ignore_list=case_ignore_list,
                                              annot_blacklist=annotation_blacklist)
    # with open('refactoring.json', 'w') as f:
    #     json.dump(aligned_docs, f)