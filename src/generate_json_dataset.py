"""
Date: 13 Nov. 2019
Author: Xiao Yang <yangx@ebi.ac.uk>
"""

from annotations import preprocess_csv_annotations, extract_text_sents
from collections import defaultdict
# from copy import deepcopy
import os
import csv
import logging
logging.basicConfig(level=logging.INFO, filename='generate-dataset.log')
logger = logging.getLogger(__name__)


def read_fulltext_sents(sentencised_path, pmcid, blacklist):
    with open(sentencised_path, 'r') as f:
        sent_xml = f.read()
    fulltext_sents = extract_text_sents(sent_xml, blacklist=blacklist)
    logger.info(f'{len(fulltext_sents)} sentences extracted from {pmcid}')
    doc_length = len(fulltext_sents)
    return fulltext_sents, doc_length


def annotation_to_set(annotations):
    """
    convert annotations to set for set operations
    :param annotations:
    :type annotations:
    :return:
    :rtype:
    """
    # convert annotation to set of annotations
    entity_set = set()
    rel_set = set()
    max_length = 0
    annos = []
    for anno in annotations:
        # print(anno)
        is_relation_tag = ('NGD' in anno['tag'] or 'YGD' in anno['tag'] or 'AMB' in anno['tag'])
        if is_relation_tag:
            if len(anno['text']) > max_length:
                rel_set = {anno['tag']}
        else:
            entity_set.add((*anno['span'], anno['text'], anno['tag']))
            annos.append((anno['text'], anno['tag'], anno['span']))
    return entity_set, rel_set


# def reannotate_type(collections):
#     """Convert WT to the correspoding tag
#     if it is false positive, tag it as FP
#     """
#     new_collections = defaultdict(list)
#     for pmcid in collections:
#         for sent in collections[pmcid]:
#             annotations = sent['ner']
#             if annotations:
#                 record = {'sid': sent['sid'], 'sent': sent['sent'], 'rel': sent['rel']}
#                 new_annotations = []
#                 for start_span, end_span, text, tag in annotations:
#                     tag = tag.upper()
#                     if 'MIS_' in tag or 'CRT_' in tag or 'WS_' in tag:
#                         tags = tag.split(',')
#                         if len(tags)==1:
#                             new_annotations.append((start_span, end_span, text, tags[0].split('_')[1]))
#                         else:
#                             if 'MIS_' in tags[0] or 'CRT_' in tags[0] or 'WS_' in tags[0]:
#                                 new_annotations.append((start_span, end_span, text, tags[0].split('_')[1]))
#                             else:
#                                 new_annotations.append((start_span, end_span, text,tags[1].split('_')[1]))
#                     if 'WT_' in tag:
#                         tags = tag.split(',')
#                         if len(tags)==1:
#                             continue
#                         else:
#                             if 'WT_' in tags[0]:
#                                 new_annotations.append((start_span, end_span, text, tags[1]))
#                             else:
#                                 new_annotations.append((start_span, end_span, text, tags[0]))
#                 if not new_annotations:
#                     continue
#                 record['annos'] = new_annotations
#                 new_collections[pmcid].append(record)
#             else:
#                 new_collections[pmcid].append({'sid': sent['sid'], 'sent': sent['sent'], 'rel': sent['rel'], 'ner': sent['ner']})
#     return new_collections


def majority_agreement(all_sent_match):
    pmcid2annos = {}
    # pmcid2rel = {}
    disagreed_entity = defaultdict(list)

    anno_count = 0
    rel_count = 0
    sent_count = 0

    _match_ent_count = 0
    _match_rel_count = 0
    for pmcid in all_sent_match:
        # print('\n' + '=========' * 5)
        # logger.debug(pmcid)
        matched_annotation = all_sent_match[pmcid]
        names_mapping = {'GROUP0': 'A', 'GROUP1': 'B', 'GROUP2': 'C'}
        doc_len = matched_annotation[0]['doc_length']

        pmcid2annos[pmcid] = {'annotations': []}
        # pmcid2rel[pmcid] = {'annotations': []}
        pmcid2annos[pmcid]['doc_len'] = doc_len
        # pmcid2rel[pmcid]['doc_len'] = doc_len

        annotations = {names_mapping[matched_annotation[i]['annotator']]: matched_annotation[i]['annotation']
                       for i in range(3)}
        anno1, anno2, anno3 = annotations['A'], annotations['B'], annotations['C']

        for sid in range(doc_len):
            entity_set1, rel_set1 = annotation_to_set(anno1.get(sid, []))
            entity_set2, rel_set2 = annotation_to_set(anno2.get(sid, []))
            entity_set3, rel_set3 = annotation_to_set(anno3.get(sid, []))

            entity_match1 = entity_set1 & entity_set2
            entity_match2 = entity_set1 & entity_set3
            entity_match3 = entity_set2 & entity_set3
            rel_match1 = rel_set1 & rel_set2
            rel_match2 = rel_set1 & rel_set3
            rel_match3 = rel_set2 & rel_set3

            majority_entity_match = entity_match1 | entity_match2 | entity_match3
            majority_rel_match = rel_match1 | rel_match2 | rel_match3

            # sometime one sentence has two different tags
            # this is used to tackle such cases
            if len(majority_rel_match) > 1:
                print(pmcid)
                print(anno1[sid][0]['sent'])
                majority_rel_match = (rel_set1 & rel_set2 & rel_set3)
                print([(anno['text'], anno['tag'], anno['pattern']) for anno in anno1[sid] if anno['tag'] in ('YGD', 'NGD')])
                print("majority vote fails due to duplicate tags")
                print(rel_set1, rel_set2, rel_set2)
                print(majority_rel_match)
                if len(majority_rel_match) > 1:
                    print("can't resove the relationship tag")
                    majority_rel_match = {'NGD'}
                    print("majority vote resolves to 'NGD' due to duplicate")

            # sometimes tag may be none
            _remove = []
            for ent in majority_entity_match:
                if not ent[-1]:
                    _remove.append(ent)
                else:
                    if ent[-1] not in ['DS', 'OG', 'GP']:
                        raise ValueError(f"unexpected tag {ent}")
            for ent in _remove:
                majority_entity_match.remove(ent)

            if sid in anno1:
                sent = anno1[sid][0]['sent']
                sec = anno1[sid][0]['section']
            elif sid in anno2:
                sent = anno2[sid][0]['sent']
                sec = anno2[sid][0]['section']
            elif sid in anno3:
                sent = anno3[sid][0]['sent']
                sec = anno3[sid][0]['section']
            else:
                continue

            # _match_ent_count += len(majority_entity_match)
            # _match_rel_count += len(majority_rel_match)

            # record the disagreement
            entity_all = entity_set1 | entity_set2 | entity_set3
            disagreement = entity_all - majority_entity_match
            if disagreement:
                disagreed_entity[pmcid].append((sid, sent, disagreement))

            if len(majority_entity_match) > 0:
                # if there are entities in this sentence
                _match_ent_count += len(majority_entity_match)
                if len(majority_rel_match) > 0:
                    # if this sentence has a relationship tag
                    # set "rel=relationship tag"
                    _match_rel_count += len(majority_rel_match)
                    if len(majority_rel_match) > 1:
                        # if there are more than 1 relationship tag for the same sentence
                        # raise Error
                        # print(rel_set1, rel_set2, rel_set3)
                        # print(list(majority_rel_match))
                        raise ValueError(f"majority vote cannot resolve this...{majority_rel_match}")
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': sorted(majority_entity_match), 'rel': list(majority_rel_match)[0]})
                else:
                    # if no relationship tag, set "rel=None"
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': sorted(majority_entity_match), 'rel': None})
                anno_count += 1
                # pmcid2annos['anno_count'] = [anno_count]
            else:
                # if no entities in the sentence
                if len(majority_rel_match) > 0:
                    if list(majority_rel_match)[0] == 'YGD':
                        # however it has relationship tag 'YGD', raise Error
                        print(pmcid)
                        print(sent)
                        print(list(majority_rel_match)[0])
                        raise ValueError(f'No entities found in this sent but it has relationship YGD ')
                    else:
                        # however, it has relationship tag 'NGD' or 'AMB' set 'rel=relationship tag'
                        _match_rel_count += len(majority_rel_match)
                        pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': list(majority_rel_match)[0]})
                else:
                    # if no entities and no relationship tag, set 'rel=None'
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})

            sent_count += 1
            # pmcid2annos['sent_count'] = [sent_count]

            # if len(majority_rel_match) > 0:
            #     pmcid2rel[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'rel': list(majority_rel_match)[0]})
            #     rel_count += 1
            #     # pmcid2rel['count'] = [rel_count]
    print(_match_ent_count, _match_rel_count)

    with open('disagreement_ent.csv', 'w') as f:
        dictwriter = csv.DictWriter(f, fieldnames=['pmcid', 'sid', 'sent', 'text', 'span', 'tag'])
        dictwriter.writeheader()
        for pmcid in disagreed_entity:
            for sid, sent, disagree in disagreed_entity[pmcid]:
                # print(disagree)
                for span_s, span_e, text, tag in disagree:
                    dictwriter.writerow({'pmcid': pmcid, 'sid': sid, 'sent': sent, 'text': text, 'span': (span_s, span_e), 'tag': tag})

    return pmcid2annos


def generate_fulltext_dataset(pmcid2data, sentencised_dir, section_blacklist):
    fulltext_dataset = {}

    dir_pmcids = [fname.split('.')[0] for fname in os.listdir(sentencised_dir)]

    for pmcid in dir_pmcids:
        sents_fpath = os.path.join(sentencised_dir, f'{pmcid}.xml')
        fulltext_sents, doc_len = read_fulltext_sents(sents_fpath, pmcid, blacklist=section_blacklist)
        if doc_len != pmcid2data[pmcid]['doc_len']:
            raise ValueError("doc length is different, please check section blacklist. "
                             "Use the same section blacklist for annotation and dataset generation.")

        fulltext_dataset[pmcid] = {'annotations': []}
        fulltext_dataset[pmcid]['doc_len'] = doc_len

        if pmcid in pmcid2data:
            annoations = pmcid2data[pmcid]['annotations']
            sid2anno = {anno['sid']: anno for anno in annoations}

            for sid, sent, sec in fulltext_sents:
                if sid in sid2anno:
                    fulltext_dataset[pmcid]['annotations'].append(sid2anno[sid])
                else:
                    fulltext_dataset[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})
        else:
            # if pmcid not in annotations, just make all the sents without abels
            for sid, sent in fulltext_sents:
                fulltext_dataset[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})
    return fulltext_dataset


def generate_fulltext_dataset_single(all_sent_match, sentencised_dir, section_blacklist, annotator):
    pmcid2annos = {}
    ent_count = 0

    for pmcid in all_sent_match:
        # print('\n' + '=========' * 5)
        # logger.debug(pmcid)
        matched_annotation = all_sent_match[pmcid]
        names_mapping = {'GROUP0': 'A', 'GROUP1': 'B', 'GROUP2': 'C'}
        doc_len = matched_annotation[0]['doc_length']

        pmcid2annos[pmcid] = {'annotations': []}
        # pmcid2rel[pmcid] = {'annotations': []}
        pmcid2annos[pmcid]['doc_len'] = doc_len
        # pmcid2rel[pmcid]['doc_len'] = doc_len

        annotations = {names_mapping[matched_annotation[i]['annotator']]: matched_annotation[i]['annotation']
                       for i in range(3)}
        anno1 = annotations[annotator]
        for sid in range(doc_len):
            entity_set1, rel_set1 = annotation_to_set(anno1.get(sid, []))
            ent_count += len(entity_set1)
            # sometimes tag may be none
            _remove = []
            for ent in entity_set1:
                if not ent[-1]:
                    _remove.append(ent)
                else:
                    if ent[-1] not in ['DS', 'OG', 'GP']:
                        raise ValueError(f"unexpected tag {ent}")
            if _remove:
                print(f'remove: {_remove}')
            for ent in _remove:
                entity_set1.remove(ent)

            if sid in anno1:
                sent = anno1[sid][0]['sent']
                sec = anno1[sid][0]['section']
            else:
                continue

            if len(entity_set1) > 0:
                if len(rel_set1) > 0:
                    # if this sentence has a relationship tag
                    # set "rel=relationship tag"
                    if len(rel_set1) > 1:
                        # if there are more than 1 relationship tag for the same sentence
                        # raise Error
                        # print(rel_set1, rel_set2, rel_set3)
                        # print(list(majority_rel_match))
                        raise ValueError(f"majority vote cannot resolve this...{rel_set1}")
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': tuple(entity_set1), 'rel': list(rel_set1)[0]})
                else:
                    # if no relationship tag, set "rel=None"
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': tuple(entity_set1), 'rel': None})
                # pmcid2annos['anno_count'] = [anno_count]
            else:
                # if no entities in the sentence
                if len(rel_set1) > 0:
                    if list(rel_set1)[0] == 'YGD':
                        # however it has relationship tag 'YGD', raise Error
                        print(pmcid)
                        print(sent)
                        print(list(rel_set1)[0])
                        raise ValueError(f'No entities found in this sent but it has relationship YGD ')
                    else:
                        # however, it has relationship tag 'NGD' or 'AMB' set 'rel=relationship tag'
                        pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': list(rel_set1)[0]})
                else:
                    # if no entities and no relationship tag, set 'rel=None'
                    pmcid2annos[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})

    fulltext_dataset = {}

    dir_pmcids = [fname.split('.')[0] for fname in os.listdir(sentencised_dir) if '.xml' in fname]

    for pmcid in dir_pmcids:
        sents_fpath = os.path.join(sentencised_dir, f'{pmcid}.xml')
        fulltext_sents, doc_len = read_fulltext_sents(sents_fpath, pmcid, blacklist=section_blacklist)
        if doc_len != pmcid2annos[pmcid]['doc_len']:
            raise ValueError("doc length is different, please check section blacklist. "
                             "Use the same section blacklist for annotation and dataset generation.")

        fulltext_dataset[pmcid] = {'annotations': []}
        fulltext_dataset[pmcid]['doc_len'] = doc_len

        if pmcid in pmcid2annos:
            annoations = pmcid2annos[pmcid]['annotations']
            sid2anno = {anno['sid']: anno for anno in annoations}

            for sid, sent, sec in fulltext_sents:
                if sid in sid2anno:
                    fulltext_dataset[pmcid]['annotations'].append(sid2anno[sid])
                else:
                    fulltext_dataset[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})
        else:
            # if pmcid not in annotations, just make all the sents without abels
            for sid, sent in fulltext_sents:
                fulltext_dataset[pmcid]['annotations'].append({'sid': sid, 'sent': sent, 'section': sec, 'ner': None, 'rel': None})
    print(f'ent count {ent_count}')
    return fulltext_dataset


def extract_annos(annos):
    new_annos = []
    for anno in annos:
        new_anno = dict(anno)
        del new_anno['sent']
        new_annos.append(new_anno)
    return new_annos


def generate_fulltext_raw_dataset(all_sent_match, sentencised_dir, section_blacklist):
    dir_pmcids = [fname.split('.')[0] for fname in os.listdir(sentencised_dir)]
    pmcid2annos = {}

    for pmcid in dir_pmcids:
        sents_fpath = os.path.join(sentencised_dir, f'{pmcid}.xml')
        fulltext_sents, doc_len = read_fulltext_sents(sents_fpath, pmcid, blacklist=section_blacklist)

        # print('\n' + '=========' * 5)
        # logger.debug(pmcid)
        matched_annotation = all_sent_match[pmcid]
        names_mapping = {'GROUP0': 'A', 'GROUP1': 'B', 'GROUP2': 'C'}
        doc_len = matched_annotation[0]['doc_length']

        pmcid2annos[pmcid] = {}
        pmcid2annos[pmcid]['doc_len'] = doc_len
        annotations = {names_mapping[matched_annotation[i]['annotator']]: matched_annotation[i]['annotation']
                       for i in range(3)}
        anno1, anno2, anno3 = annotations['A'], annotations['B'], annotations['C']

        for sid, sent, sec in fulltext_sents:
            tags_anno1 = anno1.get(sid, [])
            tags_anno2 = anno2.get(sid, [])
            tags_anno3 = anno3.get(sid, [])

            annotations1 = extract_annos(tags_anno1)
            annotations2 = extract_annos(tags_anno2)
            annotations3 = extract_annos(tags_anno3)

            #TODO: generate {sid, sent, anno1{tag1}, anno2{tag1}, anno3{tag3}}
            pmcid2annos[pmcid][sid] = {}
            pmcid2annos[pmcid][sid]['sent'] = sent
            pmcid2annos[pmcid][sid]['anno1'] = annotations1
            pmcid2annos[pmcid][sid]['anno2'] = annotations2
            pmcid2annos[pmcid][sid]['anno3'] = annotations3
            pmcid2annos[pmcid][sid]['section'] = sec

            if sid in anno1:
                sent_validate = anno1[sid][0]['sent']
            elif sid in anno2:
                sent_validate = anno2[sid][0]['sent']
            elif sid in anno3:
                sent_validate = anno3[sid][0]['sent']
            else:
                sent_validate = ''

            if sent_validate and sent_validate != sent:
                raise ValueError(f"article; {pmcid}\nsid: {sid}\nsentence from fulltext not match the sentence from annotation\n{sent}\n{sent_validate}")

    return pmcid2annos


if __name__ == "__main__":
    import json
    batch = 'full'
    annotation_dir = 'annotations/hypothesis/csv'
    sentencised_dir = 'articles/Sentencised'

    section_blacklist = ('REF', 'ACK_FUND')
    corrections = {"Marek's disease tumor":"Marek’s disease tumor"}

    case_ignore_list = ['EBV', 'BAX', 'MomL', 'ERK', 'MEK', 'SRC', 'PTEN', 'SMAD', 'ptxS', 'rpoEs', 'groES', 'groEL',
                        'nAChR', 'MYC', 'RXRα', 'MAPK', 'csrD', 'ARU', 'TRIS', 'proBDNF', 'ciaB', 'TS', 'HSPs', 'GP',
                        'BDNF', 'HTT', 'MIR', 'FES', 'TaTypA', 'VECad', 'CLIC', 'CHS', 'CAB', 'BRCA', 'NTA', 'LRE',
                        'CLOCK', 'WAVE']
    # provides another option to remove particular annotations. useful when ignore cases with regex or case ignore list
    # is too long/difficult to generate
    annotation_blacklist = ()

    all_sent_match = preprocess_csv_annotations(annotation_dir, sentencised_dir, section_blacklist,
                                                corrections, reannotate=True, ignore_list=case_ignore_list)

    # for full dataset ###
    # anno_data = majority_agreement(all_sent_match)
    #
    # dataset = generate_fulltext_dataset(anno_data, sentencised_dir=sentencised_dir,
    #                                     section_blacklist=section_blacklist)
    #
    # with open(f'ner_rel_fulltext_{batch}_allnsec.json', 'w') as f:
    #     json.dump(dataset, f)
    ######################################################

    # # for dataset of a single annotator ##
    annotator = 'A'
    single_dataset = generate_fulltext_dataset_single(all_sent_match, sentencised_dir=sentencised_dir,
                                                      section_blacklist=section_blacklist, annotator=annotator)
    with open(f'ner_rel_fulltext_{annotator}.json', 'w') as f:
        json.dump(single_dataset, f)
    ######################################################

