# Useful Scripts for Europe PMC Full Text Corpus
source codes for cleaning annotations and generating IOB files 

## Extract Annotations
- ```annotations.py```: Python script used to extract annotations from raw [Hypothes.is](https://web.hypothes.is) annotations.

The script uses regular expression to match raw annotations from sentencised XML articles, 
in particular using the ```prefix```, ```exact``` and ```suffix```. 
Raw annotations are extracted for all curators' annotations, with the exact match and the span in the sentence.


## Generate JSON dataset
- ```generate_json_dataset.py```: Python script used to extract annotations to JSON format.

From the extracted annotations, annotations are merged across all curators and only keep annotations that have a majority agreement. 
Details of generated JSON file, see [annotation details](../annotations/README.md)

## Generate IOB dataset
- ```generate_IOB_dataset.py```: Python script used to convert JSON format annotations to IOB tagging format.

Based on the generated JSON dataset file, the script generate dataset that follows IOB tagging format.
Details of generated TSV files with IOB tagging format, see [annotation details](../annotations/README.md)

## Evaluation Metrics with IOB Tagging Format
- ```metrics/ner_metrics.py```: Python script contains [SemEval evaluation metrics](https://www.cs.york.ac.uk/semeval-2013/task9/data/uploads/semeval_2013-task-9_1-evaluation-metrics.pdf).

In addition to generic Precision, Recall and F1 scores, the script allows considerations of entity overlaps between gold tags and response tags.
In particular, with metrics in 4 different settings:

1. Strict evaluation
2. Exact boundary matching: regardless to the type
3. Partial boundary matching: regardless to the type
4. Type matching: some overlap is required

Precision, recall and F1 scores are calculated based on counting occurrences of 5 cases:
- Correct (COR) : the system's output and the gold-standard annotation agree.
- Incorrect (INC) : the system's output and the gold-standard annotation disagree.
- Partial (PAR) : the systems' output and the gold-standard annotation are not identical but have some overlapping text. 
Only if partial is allowed.
- Missing (MIS) : there is a golden annotation that is not identified by the system.
- Spurius (SPU) : the system labels an entity that does not exist in the gold-standard.

Details see [SemEval evaluation metrics](https://www.cs.york.ac.uk/semeval-2013/task9/data/uploads/semeval_2013-task-9_1-evaluation-metrics.pdf).

