#!/usr/bin/env python
# coding: utf-8


# Code to split manual annotation into train, dev and valid
# Code to convert the splits into IOB format

# (c) EMBL-EBI, October 2019
#
# Started: 23 October  2019
# Updated: 28 November  2019

"""
Date: 15 Mar. 2020
Author: Santosh Tirunagari <stirunag@ebi.ac.uk>
Modified by : Xiao Yang <yangx@ebi.ac.uk>
"""

import os
# import glob
import json
import csv
import math
import random
from nltk.tokenize import wordpunct_tokenize, WordPunctTokenizer
# import copy


def extract_pmc_ids_from_jsons(json_path):
    pmcids = []
    with open(json_path, 'r') as json_file_ner_rel:
        json_data = json.loads(json_file_ner_rel.read())
        for article_id in json_data:
            pmcids.append(article_id)
    return pmcids


def create_train_dev_test_splits(json_path, percentage = 0.70):

    # percentage = 0.70
    iter = 0

    trainPMCids = []
    devPMCids = []
    testPMCids = []

    allPMCids = extract_pmc_ids_from_jsons(json_path)

    nLines = sum(1 for line in allPMCids)
    nTrain = int(nLines * percentage)
    nValid = math.floor((nLines - nTrain) / 2)
    nTest = nLines - (nTrain + nValid)

    deck = list(range(0, nLines))
    random.seed(2222)  # Please dont change the seed for the reproducibility
    random.shuffle(deck)

    train_ids = deck[0:nTrain]
    devel_ids = deck[nTrain:nTrain + nValid]
    test_ids = deck[nTrain + nValid:nTrain + nValid + nTest]

    for each_pmc_id in allPMCids:
        if iter in train_ids:
            trainPMCids.append(each_pmc_id.strip())
        elif iter in devel_ids:
            devPMCids.append(each_pmc_id.strip())
        else:
            testPMCids.append(each_pmc_id.strip())

        iter = iter + 1

    return trainPMCids, devPMCids, testPMCids


def find_sub_span(sub_span_range, full_spans_range):
    # if a sub span is present in full span return it
    # if sub_span_range[0] in range(full_spans_range[0], full_spans_range[1]):
    #     return sub_span_range

    # instead of exact match, we check if two words overlap
    if sub_span_range[0] < full_spans_range[1] and full_spans_range[0] < sub_span_range[1]:
        return sub_span_range


def convert2IOB(text_data, ner_tags):
    tokenizer = WordPunctTokenizer()

    tokens = []
    ners = []
    spans = []

    split_text = tokenizer.tokenize(text_data)
    span_text = list(tokenizer.span_tokenize(text_data))
    # for each word token append 'O'
    arr = ['O'] * len(split_text)

    if not ner_tags:
        return zip(split_text, arr)

    for each_tag in ner_tags:
        span_list = (each_tag[0], each_tag[1])
        token_list = wordpunct_tokenize(each_tag[2])
        ner_list = wordpunct_tokenize(each_tag[3])

        if len(token_list) > len(ner_list):
            ner_list = len(token_list) * ner_list

        for i in range(0, len(ner_list)):
            # The logic here is look for the first B-tag and then append I-tag next
            if i == 0:
                ner_list[i] = 'B-' + ner_list[i]
            else:
                ner_list[i] = 'I-' + ner_list[i]

        tokens.append(token_list)
        ners.append(tuple(ner_list))
        spans.append(span_list)

    split_token_span_list = list(zip(split_text, span_text))
    span_ner_list = list(zip(spans, ners))

    sub_spans = []  # get sub spans from the full spans of the ner

    for each_span_ner_list in span_ner_list:
        # in full range ner e.g., [144, 150, 'GM-CSF', 'GP']
        count = 0
        # count is to keep track of the B, I, sub tags in the ner list
        for each_token in split_token_span_list:
            sub_spans_ = find_sub_span(each_token[1], each_span_ner_list[0])
            if sub_spans_:
                sub_spans.append((sub_spans_, each_span_ner_list[1][count]))
                count = count + 1

    for i, each_span_token in enumerate(split_token_span_list):
        for each_ner_span in set(sub_spans):
            if each_span_token[1] == each_ner_span[0]:
                arr[i] = ''.join(each_ner_span[1])
    return zip(split_text, arr)


def create_train_dev_test_IOB_to_file(json_path, result_path, percentage_split = 0.70):
    trainPMCids, devPMCids, testPMCids = create_train_dev_test_splits(json_path, percentage_split)

    data_sets = ['train', 'dev', 'test']
    for ds in data_sets:
        if not os.path.exists(os.path.join(result_path, ds)):
            os.makedirs(os.path.join(result_path, ds))

    with open(json_path, 'r') as json_file_ner_rel:
        json_data = json.loads(json_file_ner_rel.read())

        for pmcid in json_data:
            if pmcid in trainPMCids:
                wpath = os.path.join(result_path, f'train/{pmcid}.tsv')
            elif pmcid in devPMCids:
                wpath = os.path.join(result_path, f'dev/{pmcid}.tsv')
            elif pmcid in testPMCids:
                wpath = os.path.join(result_path, f'test/{pmcid}.tsv')
            else:
                raise ValueError('pmcid not in any of train, dev, test set')

            with open(wpath, 'w', newline='\n') as f:
                writer = csv.writer(f, delimiter='\t', lineterminator='\n')

                for each_annotation in json_data[pmcid]['annotations']:
                    text = each_annotation['sent'].encode('utf-8').decode('utf-8')
                    ner = each_annotation['ner']
                    tagged_tokens = convert2IOB(text, ner)
                    for each_word in tagged_tokens:
                        writer.writerow(list(each_word))
                    writer.writerow('')


def create_IOB_to_file(json_path, result_path):
    with open(json_path, 'r') as json_file_ner_rel:
        json_data = json.loads(json_file_ner_rel.read())

        for pmcid in json_data:
            with open(os.path.join(result_path, f"{pmcid}.tsv"), 'w', newline='\n') as cf:
                writer = csv.writer(cf, delimiter='\t', lineterminator='\n')
                for each_annotation in json_data[pmcid]['annotations']:
                    text = each_annotation['sent'].encode('utf-8').decode('utf-8')
                    ner = each_annotation['ner']
                    tagged_tokens = convert2IOB(text, ner)
                    for each_word in tagged_tokens:
                        writer.writerow(list(each_word))
                    writer.writerow('')


def create_IOB_to_singe_file(json_path, result_path, name):
    with open(json_path, 'r') as json_file_ner_rel:
        json_data = json.loads(json_file_ner_rel.read())
        with open(os.path.join(result_path, f"{name}.tsv"), 'w', newline='\n') as cf:
            writer = csv.writer(cf, delimiter='\t', lineterminator='\n')
            for pmcid in json_data:
                for each_annotation in json_data[pmcid]['annotations']:
                    text = each_annotation['sent'].encode('utf-8').decode('utf-8')
                    ner = each_annotation['ner']
                    tagged_tokens = convert2IOB(text, ner)
                    for each_word in tagged_tokens:
                        writer.writerow(list(each_word))
                    writer.writerow('')


if __name__ == '__main__':
    print('Generating train, dev and test sets!! Please be patient')
    json_path = 'annotations/JSON/ner_rel_fulltext_full.json'
    ner_result_path = 'annotations/IOB'

    percentage_split = 0.70

    create_train_dev_test_IOB_to_file(json_path, ner_result_path, percentage_split)
    create_IOB_to_file(json_path, ner_result_path)

    # create to a single ile
    # name = 'C'
    # print('Generating train, dev and test sets!! Please be patient')
    # json_path = f'ner_rel_fulltext_{name}.json'
    # ner_result_path = './'
    # create_IOB_to_singe_file(json_path, ner_result_path, name=name)

    print('All done!! please find the files at :' + ner_result_path)
