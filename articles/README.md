# Articles from the Europe PMC Corpus
A collection of 300 full-text articles annotated in the Europe PMC corpus. All these articles are from Europe PMC Open Access subset with CC-by License.

## Original XML Articles
- ```XML/```: contains XML articles directly fetched using [Europe PMC Article Restful API](https://europepmc.org/RestfulWebService).


## Sentencised Articles
- ``Sentencised/``: contains XML articles whose text has been split into sentences using the Europe PMC sentenciser.

Articles are preprocessed by a set of Europe PMC tools such as section tagger, xslpipe and sentenciser.
A ```<SecTag>``` tag is injected by the section tagger with several different section types, such as ```ABS```, ```INTRO``` and ```METHODs``` etc. 
Then each paragraph is split into sentences by the sentenciser and a ```<SENT>``` tag is injected with a ```sid```.

Example:

```xml
<SENT sid="14" pm="."><plain>Perforations of the tympanic membrane affected 40% of children in their first 18 months of life. </plain></SENT>
```
