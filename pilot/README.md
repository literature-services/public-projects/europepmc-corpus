# Pilot Study

A collection of 3 full text articles that were used in a pilot study. 
Before the start of the Europe PMC Full Text Corpus annotation, a pilot study was done to provide feedback between curators and Europe PMC.
These was mainly used to let curators get familiar with the entire annotation process and test the annotation platform [Hypothes.is](https://web.hypothes.is).

- ```annotations/csv/```: contains raw annotations fetched from the annotation platform [Hypothes.is](https://web.hypothes.is) in comma-separated values (CSV) format.
- ```articles/```: contains the full-text articles annotated in the pilot study.
    - ```Sentencised/```: contains XML articles whose text has been split into sentences using the Europe PMC sentenciser.
    - ```XML/```: contains XML articles directly fetched using [Europe PMC Article Restful API](https://europepmc.org/RestfulWebService).
